﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProjektZamowienia
{
    public partial class Rejestracjaa : Form
    {
        /// <summary>
        /// Konstruktor pusty
        /// </summary>
        public Rejestracjaa()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Wyjście
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Zmienna maksymalnego id użytkownika w bazie
        /// </summary>
        private string max;
        /// <summary>
        /// Rejestracja użytkownika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox5.Text != "" && textBox7.Text != "")//Sparwdzenie czy są puste pola
            {
                try
                {
                    string sql = "Select Max(ID) From dbo.Uzytk";

                    Zapyt(sql, false);
                    max = dataGridView1[0, 0].Value.ToString();
                    int w = Int32.Parse(max) + 1;
                    max = w.ToString();

                    sql = "Select Imie From dbo.Uzytk Where Email = '" + textBox5.Text + "'";
                    Zapyt(sql, false);



                    if (dataGridView1[0, 0].Value == null)//Sprawdzienie czy nie ma takiego użytkownika
                    {
                        //Dodaj do tabeli w bazie nowego użytkownika
                        sql = "INSERT INTO dbo.Uzytk (ID, Imie, Nazwisko, Miasto, Email, Password, Admin) VALUES (" + max + ", '" + textBox1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + textBox5.Text + "', '" + textBox7.Text + "', 'False')";
                        Zapyt(sql, true);



                        MessageBox.Show("Zarejestrowano!");
                    }
                    else MessageBox.Show("Taki użytkownik istnieje");

            }
                catch (Exception) { MessageBox.Show("Błąd!"); }
        }
            else
                MessageBox.Show("Wszystkie pola muszą być uzupełnione");
        }
        /// <summary>
        /// Zapyt do bazy danych "Insert" oraz "Select"
        /// </summary>
        /// <param name="sql">zapyt do bazy danych</param>
        /// <param name="zap">zapyt "Insert" lub "Select"</param>
        private void Zapyt(string sql, bool zap)
        {
            if (zap == true)
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    SqlCommand sc = new SqlCommand(sql, connection);
                    connection.Open();
                    sc.ExecuteNonQuery();
                    connection.Close();
                }
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "dbo.Uzytk");
                    dataGridView1.DataSource = ds.Tables[0].DefaultView;
                }
            }
        }

        private void Rejestracjaa_Load(object sender, EventArgs e)
        {

        }
    }
}
