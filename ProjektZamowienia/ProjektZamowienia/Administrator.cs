﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProjektZamowienia
{
    public partial class Administrator : Form
    {
        /// <summary>
        /// Konstruktor pusty
        /// </summary>
        public Administrator()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Zminna która przechowuje w sobie imię użytkownika
        /// </summary>
        public string Lab;
        /// <summary>
        /// Id użytkownika
        /// </summary>
        public string Id;
        /// <summary>
        /// Przy ładołaniu okna, zapisuje do Label1 Imie użytkownika i wypełnia DataGrideView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Administrator_Load(object sender, EventArgs e)
        {
            label1.Text = Lab;
            DataRefresh();
        }
        /// <summary>
        /// Wypełnia DataGrideView
        /// </summary>
        private void DataRefresh()
        {

            using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
            {
                string sql;
                for (int i = 0; i < 3; i++)
                {
                    if (i == 0)
                        sql = "Select IDT, Towar From dbo.Towary";
                    else if(i == 1)
                        sql = "Select ID, Imie, Nazwisko, Miasto, Email, Password, Admin From dbo.Uzytk";
                    else
                        sql = "Select ID, Zamowienie, Podtwierdzenie From dbo.Zamowienie";

                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();


                    switch (i)
                    {
                        case 0:
                            da.Fill(ds, "dbo.Towary");
                            break;
                        case 1:
                            da.Fill(ds, "dbo.Uzytk");
                            break;
                        case 2:
                            da.Fill(ds, "dbo.Zamowienie");
                            break;
                        default:
                            break;
                    }

                    if (i == 0)
                        dataGridView4.DataSource = ds.Tables[0].DefaultView;
                    else if(i == 1)
                        dataGridView3.DataSource = ds.Tables[0].DefaultView;
                    else
                        dataGridView1.DataSource = ds.Tables[0].DefaultView;
                }
            }
        }

        /// <summary>
        /// Zmienia dane tabeli "Towary"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    string sql = "Delete From dbo.Towary";
                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "dbo.Towary");

                    foreach (DataGridViewRow row in dataGridView4.Rows)
                    {
                        try
                        {
                            sql = "INSERT INTO dbo.Towary (IDT, Towar) VALUES (" + Int32.Parse(row.Cells[0].Value.ToString()) + ", '" + row.Cells[1].Value.ToString() + "')";
                            SqlCommand cp = new SqlCommand(sql, connection);
                            connection.Open();
                            cp.ExecuteNonQuery();
                            connection.Close();
                        }
                        catch { }
                    }
                    MessageBox.Show("Dane dodane");
                }
            }
            catch { MessageBox.Show("Błąd połączenia"); }
        }
        /// <summary>
        /// Odswierzanie danych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void odswieżDaneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataRefresh();
        }
        /// <summary>
        /// Przejście do okna Zamówień
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zamówieniaUżytkownikówToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Zamowienia zam = new Zamowienia();
            zam.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Zmienia dane tabeli "Uzytk"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    string sql = "Delete From dbo.Uzytk";
                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "dbo.Uzytk");

                    foreach (DataGridViewRow row in dataGridView3.Rows)
                    {
                        try
                        {
                            sql = "INSERT INTO dbo.Uzytk (ID, Imie, Nazwisko, Miasto, Email, Password, Admin) VALUES (" + Int32.Parse(row.Cells[0].Value.ToString()) + ", '" + row.Cells[1].Value.ToString() + "','" + row.Cells[2].Value.ToString() + "','" + row.Cells[3].Value.ToString() + "','" + row.Cells[4].Value.ToString() + "','" + row.Cells[5].Value.ToString() + "','" + row.Cells[6].Value.ToString() + "')";
                            SqlCommand cp = new SqlCommand(sql, connection);
                            connection.Open();
                            cp.ExecuteNonQuery();
                            connection.Close();
                        }
                        catch { }
                    }
                    MessageBox.Show("Dane dodane");
                }
            }
            catch { MessageBox.Show("Błąd połączenia"); }
        }
        /// <summary>
        /// Zmienia dane tabeli "Zamowienie"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    string sql = "Delete From dbo.Zamowienie";
                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "dbo.Zamowiene");

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        try
                        {
                            sql = "INSERT INTO dbo.Zamowienie (ID, Zamowienie, Podtwierdzenie) VALUES (" + Int32.Parse(row.Cells[0].Value.ToString()) + ", '" + row.Cells[1].Value.ToString() + "','" + row.Cells[2].Value.ToString() + "')";
                            SqlCommand cp = new SqlCommand(sql, connection);
                            connection.Open();
                            cp.ExecuteNonQuery();
                            connection.Close();
                        }
                        catch { }
                    }
                    MessageBox.Show("Dane zmienione");
                }
            }
            catch { MessageBox.Show("Błąd połączenia"); }
        }
        /// <summary>
        /// Zmiana użytkownika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zmianaUżytkownikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            Close();
        }
        /// <summary>
        /// Wyjście
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wyjścieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
