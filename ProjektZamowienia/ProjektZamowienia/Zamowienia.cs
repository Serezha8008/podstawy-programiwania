﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProjektZamowienia
{
    public partial class Zamowienia : Form
    {
        public Zamowienia()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Id Użytkownika
        /// </summary>
        public string IdUz;
        /// <summary>
        /// Zapyt do bazy SQL
        /// </summary>
        /// <param name="sql">Zmienna z zapytem</param>
        public void Zamoww(string sql)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "dbo.Zamowienie");
                    dataGridView1.DataSource = ds.Tables[0].DefaultView;
                }
            }
            catch { MessageBox.Show("Błąd"); }
        }
        /// <summary>
        /// Jeżeli to okno odkrył admin, on może przeczytać wszystkie zamówienia. Jeżeli zwykły użtkownik, to tylko swoje zamówienia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Zamowienia_Load(object sender, EventArgs e)
        {
            if (IdUz == "" || IdUz == null)
                Zamoww("Select ID, Zamowienie, Podtwierdzenie From dbo.Zamowienie");
            else
                Zamoww("Select ID, Zamowienie, Podtwierdzenie From dbo.Zamowienie Where ID = '" + IdUz + "'");
        }
    }
}
