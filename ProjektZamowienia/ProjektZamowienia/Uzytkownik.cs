﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProjektZamowienia
{
    public partial class Uzytkownik : Form
    {
        /// <summary>
        /// Konstruktor pusty
        /// </summary>
        public Uzytkownik()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Przejście do okna zamówień
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mojeZamowieniaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Zamowienia zam = new Zamowienia();
            zam.IdUz = Id;
            zam.Show();
        }
        /// <summary>
        /// Zminna która przechowuje w sobie imię użytkownika
        /// </summary>
        public string Lab;
        /// <summary>
        /// Id użytkownika
        /// </summary>
        public string Id;
        /// <summary>
        /// maksymalny Id w bazie
        /// </summary>
        private string max = "";
        /// <summary>
        /// Ilość klików po przycisku "Dodaj"
        /// </summary>
        private int dod = -1;
        /// <summary>
        /// Dodanie towaru do listy zamówień
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            int index = textBox1.Text.IndexOf(comboBox1.SelectedItem.ToString());
            if (index <= -1)
            { textBox1.Text += comboBox1.SelectedItem.ToString() + ", "; dod++; }
            else MessageBox.Show("Artukuł już istneje");
        }
        /// <summary>
        /// Usuwanie towaru z listy zamówień
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (dod > -1)
            {
                string[] towars = Towary(dod + 1);

                for (int i = 0; i <= dod; i++)
                {
                    if (" " + comboBox1.SelectedItem.ToString() == towars[i])
                    {
                        towars[i] = "";

                        textBox1.Text = ",";

                        for (int q = 0; q <= dod; q++)
                            if (towars[q] != "")
                                textBox1.Text += towars[q] + ",";

                        textBox1.Text += " ";
                        dod--;

                        break;
                    }
                }
            }
        }
        /// <summary>
        /// Zamów z listy towarów
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    string sql = "INSERT INTO dbo.Zamowienie (ID, Zamowienie, Podtwierdzenie) VALUES ('" + Id + "', '" + textBox1.Text + "', 'False')";
                    SqlCommand sc = new SqlCommand(sql, connection);
                    connection.Open();
                    sc.ExecuteNonQuery();
                    connection.Close();
                }
                MessageBox.Show("Zamówiono");
            }
            catch { MessageBox.Show("Nie udało się zamówić"); }
        }
        /// <summary>
        /// Zapisuje wszystie wybrane produkty do tablicy
        /// </summary>
        /// <param name="dod">Ilość klików po przycisku "Dodaj"</param>
        /// <returns>Zwraca tablicę towarów</returns>
        private string[] Towary(int dod)
        {
            string text = textBox1.Text;
            int substring, length;
            string[] towars = new string[dod + 1];

            for (int i = 0; i <= dod; i++)
            {

                towars[i] = "";
                substring = text.IndexOf(", ");

                length = text.Length;
                text = text.Substring(substring + 1, length - (substring + 1));

                substring = text.IndexOf(", ");
                if (substring > -1)
                    towars[i] = text.Substring(0, substring);

            }

            return towars;
        }
        /// <summary>
        /// Zapyt Select do bazy danych
        /// </summary>
        /// <param name="sql">Zapyt SQL do bazy danych</param>
        private void Zapyt(string sql)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "dbo.Towary");
                    dataGridView1.DataSource = ds.Tables[0].DefaultView;
                }
            }
            catch { MessageBox.Show("Błąd"); }
        }
        /// <summary>
        /// Po ładowaniu okna wypisuje Imie do Label1, Wypełnia ComboBox1 danymi z DataGrideView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Uzytkownik_Load(object sender, EventArgs e)
        {
            label1.Text = Lab;
            textBox1.Text = ", ";

            Zapyt("Select Max(IDT) From dbo.Towary");
            max = dataGridView1[0, 0].Value.ToString();

            Zapyt("Select Towar Admin From dbo.Towary");


            List<string> dane = new List<string>();
            for (int i = 0; i <= Int32.Parse(max); i++)
                dane.Add(dataGridView1[0, i].Value.ToString());

            comboBox1.DataSource = dane;
        }
        /// <summary>
        /// Zmiana użytkownika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zmianaUżytkownikaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            Close();
        }
        /// <summary>
        /// Wyjście
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wyjścieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
