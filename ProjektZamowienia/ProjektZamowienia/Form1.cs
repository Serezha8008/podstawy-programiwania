﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProjektZamowienia
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Konstruktor pusty
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Logowanie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")//Jeżeli pola nie są puste
            {
                string sql = "Select Imie, ID, Admin From dbo.Uzytk Where Email = '" + textBox1.Text + "' AND Password = '" + textBox2.Text + "'";
                using (SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-C8BO9BR;Initial Catalog=ProjektZamowienia;Integrated Security=True"))
                {
                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    SqlCommandBuilder cd = new SqlCommandBuilder(da);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "dbo.Uzytk");
                    dataGridView1.DataSource = ds.Tables[0].DefaultView;//Zapis do DataGridView Imie, Id i status administratora wpisanego użytkownika


                    try
                    {
                        if (dataGridView1[0, 0].Value.ToString() != "" && dataGridView1[2, 0].Value.ToString() == "False")//Użytkownik istnieje i posiada status usera
                        {
                            Uzytkownik okno = new Uzytkownik();
                            okno.Lab = dataGridView1[0, 0].Value.ToString();
                            okno.Id = dataGridView1[1, 0].Value.ToString();
                            okno.Show();

                            this.Visible = false;
                        }
                        else if (dataGridView1[0, 0].Value.ToString() != "" && dataGridView1[2, 0].Value.ToString() == "True")//Użytkownik istnieje i posiada status administratora
                        {
                            Administrator admin = new Administrator();
                            admin.Lab = dataGridView1[0, 0].Value.ToString();
                            admin.Id = dataGridView1[1, 0].Value.ToString();
                            admin.Show();
                            this.Visible = false;
                        }
                    }
                    catch (Exception) { MessageBox.Show("Błędne dane"); }
                }
            }
            else MessageBox.Show("Puste Pola!!");
        }
        /// <summary>
        /// Przejście do okna "Rejestracja"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Rejestracjaa rej = new Rejestracjaa();
            rej.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
